import os


HOME_DIR = os.environ['HOME']
JETBRAIN_DIR = 'clion/clion-2021.1.3'
JETBRAIN_CONFIG_DIR = 'JetBrains/CLion2021.1'



os.chdir(f"{HOME_DIR}/.config/{JETBRAIN_CONFIG_DIR}/eval/")

os.system("find . -maxdepth 1 -type f -name \"*.key\" -delete")


os.chdir(f"{HOME_DIR}/{JETBRAIN_DIR}/bin/")
os.system("bash clion.sh")
